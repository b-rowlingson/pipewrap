
context("Test basics")

context("Row and column loggers")
rowlog <- before_after_wrap("Rows: Before: %s, After: %s", nrow)
collog <- before_after_wrap("Cols: Before: %s, After: %s", ncol)


rf = rowlog(filter)
cf = collog(filter)

rs = rowlog(select)
cs = collog(select)

expect_message(iris %>% rf(Species == "setosa"),"Rows: Before: 150, After: 50")
expect_message(iris %>% cf(Species == "setosa"),"Cols: Before: 5, After: 5")

expect_message(iris %>% rs(Species, Sepal.Length),"Rows: Before: 150, After: 150")
expect_message(iris %>% cs(Species, Sepal.Length),"Cols: Before: 5, After: 2")

expect_message(iris %>% (filter %>% rowlog)(Sepal.Length>mean(Sepal.Length)),"Rows: Before: 150, After: 70")

mw = pipemessage

expect_message(iris %>%
    mw("Selecting...") %>% select(Sepal.Length, Sepal.Width, Species),
               "Selecting...")




