# pipewrap

Make some functions to put in your pipes.

# Example

Suppose you have a pipe like this:

```
iris %>% filter(Species == "setosa") %>% filter(Sepal.Length > 5)
```

and you want to know what each filter has done to your data. Use a `before_after_wrap`
function to create an instrumented `filter` function that logs some information.

```
> ffilter = before_after_wrap("Rows %s -> %s", nrow)(filter)
> iris %>% ffilter(Species == "setosa") %>% ffilter(Sepal.Length > 5) %>% dim()

Rows 150 -> 50
Rows 50 -> 22
[1] 22  5

```

This will run the filter and also log (using `message`) the change in the rows.

If you want to also output a message as the filter progresses, use `pipemessage`:

```
> iris %>% pipemessage("Species:") %>% 
         ffilter(Species == "setosa") %>% 
  	 pipemessage("Largest:") %>%
         ffilter(Sepal.Length > 5) %>% dim()

Species:
Rows 150 -> 50
Largest:
Rows 50 -> 22
[1] 22  5
```

# Pipe timing.

The `pipetimer` function creates a time object that you can stick in your
pipes to see how long its been since the last time this time object was piped
through. You can also add an informational message.

Suppose you want to know how long a select and a filter operation take. Just 
stick this in your pipe:

```
> t1 = pipetimer()
> iris %>% t1(status="reset") %>%
           select(Petal.Width, Species) %>% t1("select")  %>% 
           filter(Petal.Width >2.3) %>% t1("filter")
Started... Timer
Duration (select): 0.00112128257751465
Duration (filter): 0.00100469589233398
  Petal.Width   Species
1         2.5 virginica
2         2.5 virginica
3         2.4 virginica
4         2.4 virginica
5         2.4 virginica
6         2.5 virginica
```

You can re-use the timer object if you reset it at the start of a pipe.
