##' pipify a function
##'
##' given some function, make a version with an extra .data argument. This
##' function can then be used for its side effects (like messaging, saving)
##' in a pipeline.
##' 
##' @title pipify a function
##' @param f the function to pipify
##' @return a function ready to use in pipes
##' @author Barry Rowlingson
##' @examples
##'  whenisit = pipify(function(...){message(Sys.time())})
##'  iris %>% whenisit %>% filter(Sepal.Length > 5) %>% dim
##' @export
pipify <- function(f){
    g = function(.data,...){
        f(...)
        return(.data)
    }
    g
}
##' Output a message in a pipe
##'
##' Enables the use of the message function in a pipeline.
##' @title Use message in a pipe
##' @param .data The data going through the pipe.
##' @param ... Text of message, and other arguments passed to the message() function.
##' @return The data, unchanged.
##' @seealso message
##' @author Barry Rowlingson
##' @export

pipemessage = pipify(message)

##' Output a warning message in a pipeline.
##'
##' This lets you put warning messages into pipelines.
##' @title Emit a warning in a pipeline.

##' @param .data The data going through the pipe.
##' @param ... The warning message, and other parameters passed to the message() function
##' @return The data, unchanged
##' @seealso message
##' @author Barry Rowlingson
##' @export

pipewarning = pipify(warning)


